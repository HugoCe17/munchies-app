import oauthSignature from 'oauth-signature'
import _ from 'lodash'
import qs from 'querystring'
var n = require('../nonce')();

var url = "https://api.yelp.com/v2/search"

/**
 * Creates a URL for GET requests
 * _parameters: Object = {coordinate, term}
 */
export function createYelpAPICallURL(_parameters) {

    if (_parameters) {
        let url = "https://api.yelp.com/v2/search",

            httpMethod = 'GET',

            default_parameters = {
                // ll: '37.77493,-122.419415',
                ll: `${_parameters.coordinate.latitude},${_parameters.coordinate.longitude}`  
            },

            set_parameters = {
                term: `${_parameters.term}`
            },

            required_parameters = {
                oauth_consumer_key: "-0nFbICWTmrt8eI8WZKmeA",
                oauth_token: "Kck3C_9IAFKjJiM6f6serrbDbvvEylux",
                oauth_nonce: n(),
                oauth_timestamp: n().toString().substr(0, 10),
                oauth_signature_method: 'HMAC-SHA1',
                oauth_version: '1.0'
            },

            optional_secrets = {
                oauth_token_secret: "75CWg97iobmqj6VqW5sqyx61LKc",
                oauth_consumer_secret: "3WQ8Ps8Rmv110risRK6yqe1IV9Q",
            }

        var parameters = _.assign(default_parameters, set_parameters, required_parameters);
        var signature = oauthSignature.generate(httpMethod, url, parameters, optional_secrets.oauth_consumer_secret, optional_secrets.oauth_token_secret, { encodeSignature: false });
        parameters.oauth_signature = signature;
        var paramURL = qs.stringify(parameters);
        var apiURL = url + '?' + paramURL;

        return apiURL;

    } else {
        console.err("Missing Parameters")
    }
}
