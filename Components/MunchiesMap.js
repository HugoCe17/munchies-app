
import React, {
    Component,
    StyleSheet,
    Text,
    TextInput,
    View,
    ScrollView,
    PropTypes,
    Platform,
    Animated,
    Dimensions,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';

import MapView from 'react-native-maps';
// import SearchBar from 'react-native-search-bar'

import { createYelpAPICallURL } from '../lib/yelp'
import { dismissKeyboard } from '../lib/helpers'

let { width, height } = Dimensions.get('window');

const SCREEN_HEIGHT = height,
    SCREEN_WIDTH = width,
    ASPECT_RATIO = width / height,
    LATITUDE_DELTA = 0.09,
    LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO,
    RADIUS = 1500,
    SPACE = 0.01;


export default class MunchiesMap extends Component {

    constructor(props) {
        super(props)
        this.state = {
            places: [],
            span: {
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            term: '',
            callout: {
                title: '',
                description: ''
            }
        }

        this._getMunchiesLocationYelp = this._getMunchiesLocationYelp.bind(this)
        this._renderMarkers = this._renderMarkers.bind(this)

    }

    _renderIOSSearchBar() {
        var SearchBar = require('react-native-search-bar');
        return <SearchBar
            ref='searchBar'
            placeholder='Search Location'
            barTintColor={"rgba(43, 81, 134, 1)"}
            onChangeText={(term) => this.setState({ term }) }
            onSearchButtonPress={ this._getMunchiesLocationYelp }/>
    }

    _renderAndroidSearchBar() {
        return <TextInput
            style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
            onChangeText={(term) => this.setState({ term }) }
            onSubmitEditing={ this._getMunchiesLocationYelp }
            blurOnSubmit={true}/>
    }

    _renderMarkers() {
        return this.state.places.map((place, index, arr) => {
            return <MapView.Marker
                coordinate={place.location.coordinate}
                title={place.name}
                description={place.snippet_text}
                key={`${index}-${place.name}`}/>
        })
    }

    async _getMunchiesLocationYelp() {
        // dismiss keyboard upon search

        if (Platform.OS === 'ios')
            this.refs.searchBar.blur();

        if (this.state.term.length > 0) {
            let parameters = {
                term: this.state.term,
                coordinate: {
                    latitude: this.props.coordinate.latitude,
                    longitude: this.props.coordinate.longitude
                }
            }

            let yelpURL = createYelpAPICallURL(parameters)
            try {

                let response = await fetch(yelpURL);
                let responseJSON = await response.json();

                this.setState({
                    places: responseJSON.businesses,
                    span: {
                        latitudeDelta: responseJSON.region.span.latitude_delta,
                        longitudeDelta: responseJSON.region.span.longitude_delta
                    }
                })

            } catch (err) {
                console.error(err)
            }
        }
    }

    render() {
        let InputSearchBar = Platform.OS === 'android'
            ? this._renderAndroidSearchBar()
            : this._renderIOSSearchBar()

        let {latitude, longitude} = this.props.coordinate
        let {latitudeDelta, longitudeDelta} = this.state.span

        let region = {
            latitude,
            longitude,
            latitudeDelta,
            longitudeDelta,
        }


        return (
            <View style={styles.container}>
                <View style={styles.searchbar}>
                    { InputSearchBar }
                </View>

                <View style={styles.map}>
                    <MapView
                        style={ styles.mapContainer }
                        region={ region }
                        showsUserLocation={true}
                        showsScale={true}>
                        { this._renderMarkers() }
                    </MapView>
                </View>

                <View style={styles.buttonsContainer}>
                    <TouchableOpacity style={styles.button}>

                        <Text style={styles.buttonText}>Distance</Text>

                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}>

                        <Text style={styles.buttonText}>Random</Text>

                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}>

                        <Text style={styles.buttonText}>Favorite</Text>

                    </TouchableOpacity>
                </View>


            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    searchbar: {
        justifyContent: 'flex-start'
    },
    mapContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    map: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    buttonsContainer: {
        flexDirection: 'row'
    },
    button: {
        backgroundColor: 'rgba(43, 81, 134, 1)',
        width: SCREEN_WIDTH / 3,
    },
    buttonText: {
        textAlign: 'center',
        fontSize: 23,
        color: '#fff',
    }
});
