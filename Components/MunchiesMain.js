
import React, {
    Component,
    StyleSheet,
    Text,
    View,
    PropTypes,
    Dimensions,
    TouchableOpacity,
    StatusBar
} from 'react-native';

import MunchiesMap from './MunchiesMap'

export default class MunchiesMain extends Component {
    constructor(props) {
        super(props)
        this.watchID = undefined
        this.state = {
            coordinate: {
                latitude: 0,
                longitude: 0
            }
        }
    }

    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            (location) => {
                let {latitude, longitude} = location.coords
                this.setState({
                    coordinate: {
                        latitude,
                        longitude
                    }
                })

            },
            (err) => alert(err.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
        this.watchID = navigator.geolocation.watchPosition((position) => {
            var lastPosition = JSON.stringify(position);
            this.setState({ lastPosition });
        });

    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID)
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    hidden={true} />
                <MunchiesMap coordinate={this.state.coordinate} />
            </View>
        )
    }
}