/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    MapView,
    TouchableOpacity
} from 'react-native';

import MunchiesMain from './components/MunchiesMain'

class MunchiesApp extends Component {
    render() {
        return (
            <MunchiesMain/>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    }
})


AppRegistry.registerComponent('MunchiesApp', () => MunchiesApp);
