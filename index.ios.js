/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    MapView,
    TouchableOpacity
} from 'react-native';

import MunchiesMain from './components/MunchiesMain'

class MunchiesApp extends Component {
    render() {
        return (
            <MunchiesMain/>
        );
    }
}


AppRegistry.registerComponent('MunchiesApp', () => MunchiesApp);
